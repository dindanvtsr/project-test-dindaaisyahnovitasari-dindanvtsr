import { combineReducers } from 'redux'
import postReducer, {
  fetchPostStart,
  fetchPostSuccess,
  fetchPostFailure,
} from '../reducers/listPostReducer'

export {
  fetchPostStart,
  fetchPostSuccess,
  fetchPostFailure,
} from '../reducers/listPostReducer'

const rootReducer = combineReducers({
  post: postReducer,
})

export default rootReducer
