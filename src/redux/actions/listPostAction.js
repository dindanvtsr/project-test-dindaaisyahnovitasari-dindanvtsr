import axios from 'axios'
import {
  fetchPostStart,
  fetchPostSuccess,
  fetchPostFailure,
} from '../reducers/listPostReducer'

export const fetchPost =
  (
    page = 1,
    size = 10,
    append = ['small_image', 'medium_image'],
    sort = '-published_at'
  ) =>
  async (dispatch) => {
    try {
      dispatch(fetchPostStart())

      const response = await axios.get(
        `https://suitmedia-backend.suitdev.com/api/ideas`,
        {
          params: {
            'page[number]': page,
            'page[size]': size,
            append: append,
            sort: sort,
          },
        }
      )

      dispatch(fetchPostSuccess(response?.data))
    } catch (error) {
      dispatch(fetchPostFailure(error.message))
    }
  }
