import React from 'react'
import Navbar from '../components/Navbar'
import './UnderMaintenance.css'

export default function About() {
  return (
    <div>
      <Navbar />
      <div className="picture">
        <img
          src="./under_construction.png"
          width={600}
          alt="Under Maintenance"
        />
      </div>
      <div className="textContainer">
        <h1>Under Maintenance</h1>
        <p className="description">
          We are currently working on awesome new page
        </p>
      </div>
    </div>
  )
}
