import React, { useEffect, useState } from 'react'
import Navbar from '../components/Navbar'
import './Ideas.css'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { Grid, CardActionArea } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPost } from '../redux/actions/listPostAction'
import Pagination from '@mui/material/Pagination'

export default function Ideas() {
  const dispatch = useDispatch()
  const { data, loading, error } = useSelector((state) => state.post)
  const [currentPage, setCurrentPage] = useState(1)

  const handlePageChange = (event, page) => {
    setCurrentPage(page)
  }

  useEffect(() => {
    dispatch(fetchPost(currentPage))
  }, [dispatch, currentPage])

  if (loading) {
    return <div>Loading...</div>
  }

  if (error) {
    return <div>Error: {error}</div>
  }

  return (
    <div>
      <Navbar />
      <div className="heroContainer">
        <img src="./Ideas.jpg" className="banner" alt="Ideas" />
        <div className="overlay">
          <h1>Ideas</h1>
          <p>Where all our great things begin</p>
        </div>

        <div className="triangleContainer">
          <div className="triangle"></div>
        </div>
      </div>
      <div className="contentContainer">
        <p>
          Showing {data?.meta?.from} - {data?.meta?.to} of {data?.meta?.total}
        </p>

        <Grid container spacing={3}>
          {data?.data?.slice(0, 10).map((newsPost) => (
            <Grid key={newsPost.id} item xs={12} sm={6} md={3}>
              <Card sx={{ maxWidth: 250, minHeight: 320 }}>
                <CardActionArea>
                  <div>
                    {newsPost?.medium_image?.map((image, index) => (
                      <CardMedia
                        key={index}
                        component="img"
                        height="140"
                        style={{
                          height: '140px',
                          width: '100%',
                          backgroundImage: `url("${image.url}")`,
                          backgroundSize: 'cover',
                        }}
                        loading="lazy"
                      />
                    ))}
                    <CardContent>
                      <p style={{ color: 'grey', marginBottom: '-10px' }}>
                        {new Date(newsPost.published_at).toLocaleDateString(
                          'en-GB',
                          {
                            day: 'numeric',
                            month: 'long',
                            year: 'numeric',
                          }
                        )}
                      </p>
                      <h3
                        style={{
                          display: '-webkit-box',
                          WebkitBoxOrient: 'vertical',
                          overflow: 'hidden',
                          WebkitLineClamp: 3,
                          textOverflow: 'ellipsis',
                        }}
                      >
                        {newsPost.title}
                      </h3>
                    </CardContent>
                  </div>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>

        <div className="page">
          <Pagination
            count={data?.meta?.last_page || 1}
            page={currentPage}
            onChange={handlePageChange}
            shape="rounded"
            color="warning"
          />
        </div>
      </div>
    </div>
  )
}
