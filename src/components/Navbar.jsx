import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import './Navbar.css'

export default function Navbar() {
  const [show, setShow] = useState(true)
  const [activeLink, setActiveLink] = useState('')

  const controlNavbar = () => {
    if (window.scrollY > 250) {
      setShow(false)
    } else {
      setShow(true)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', controlNavbar)
    return () => {
      window.removeEventListener('scroll', controlNavbar)
    }
  }, [])

  const handleNavLinkClick = (link) => {
    setActiveLink(link)
  }

  return (
    <div className={`nav ${show && 'navbarColor'}`}>
      {show && (
        <div className="container">
          <div className="logo">
            <img src="./suitmedia.png" width={180} alt="Suitmedia" />
          </div>
          <div className="navText">
            <NavLink
              to="/"
              className={`navbarText ${activeLink === 'Work' && 'active'}`}
              onClick={() => handleNavLinkClick('Work')}
            >
              Work
            </NavLink>
            <NavLink
              to="/about"
              className={`navbarText ${activeLink === 'About' && 'active'}`}
              onClick={() => handleNavLinkClick('About')}
            >
              About
            </NavLink>
            <NavLink
              to="/services"
              className={`navbarText ${activeLink === 'Services' && 'active'}`}
              onClick={() => handleNavLinkClick('Services')}
            >
              Services
            </NavLink>
            <NavLink
              to="/ideas"
              className={`navbarText ${activeLink === 'Ideas' && 'active'}`}
              onClick={() => handleNavLinkClick('Ideas')}
            >
              Ideas
            </NavLink>
            <NavLink
              to="/careers"
              className={`navbarText ${activeLink === 'Careers' && 'active'}`}
              onClick={() => handleNavLinkClick('Careers')}
            >
              Careers
            </NavLink>
            <NavLink
              to="/contact"
              className={`navbarText ${activeLink === 'Contact' && 'active'}`}
              onClick={() => handleNavLinkClick('Contact')}
            >
              Contact
            </NavLink>
          </div>
        </div>
      )}
    </div>
  )
}
